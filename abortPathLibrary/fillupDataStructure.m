function [ pathMap ] = fillupDataStructure(globalPathList,globalPathIds,mapRes,minX,minY,maxX,maxY,maxState,minState,stateResolution)
%FILLUPDATASTRUCTURE Summary of this function goes here
%   Detailed explanation goes here

%% INITIALIZE MAP
mapSize = coord2index(maxX,maxY,mapRes,minX,minY);
stateId = value2Id(maxState,minState,stateResolution);

for i=1:mapSize(1)
    for j=1:mapSize(2)
        for k=1:stateId(1)
            for l=1:stateId(2)
                for m=1:stateId(3)
                    pathMap(i,j).state(k,l,m).pathIds = [];
                end
            end        
        end
    end
end

%% FILL UP THE MAP
for i=1:size(globalPathIds,1)
    firstID = globalPathIds(i);
    if i<size(globalPathIds,1)
        lastID =  globalPathIds(i+1)-1;
    else
        lastID = size(globalPathList,1);
    end
    state = [globalPathList(firstID,end-1) globalPathList(firstID,end) globalPathList(firstID,end-2)];
    stateId = value2Id(state,minState,stateResolution);
    
    IDs = coord2index(globalPathList(firstID:lastID,2),globalPathList(firstID:lastID,3),mapRes,minX,minY);
    linearIDs = sub2ind(mapSize, IDs(:,1), IDs(:,2));
    linearIDs = unique(linearIDs);
    IDs = [];
    [IDs(:,1),IDs(:,2)] = ind2sub(mapSize,linearIDs);
        
    for j=1:size(IDs,1)
        pathMap(IDs(j,1),IDs(j,2)).state(stateId(1),stateId(2),stateId(3)).pathIds = [pathMap(IDs(j,1),IDs(j,2)).state(stateId(1),stateId(2),stateId(3)).pathIds;[firstID,lastID]];
    end    
end
end

