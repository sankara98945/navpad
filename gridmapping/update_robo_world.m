function robo_map = update_robo_world(angle_range,angle_resolution,range_resolution,x,y,r,collision,position,yaw,robo_map,map_struct,grid_params)

for j=1:size(position,1)
    angles = angle_range(1):angle_resolution:angle_range(2);
    angles = bsxfun(@plus, angles,yaw(j));
    for i=1:size(angles,2)
        ranges = [0:range_resolution:r(j,i)]';
        xy = bsxfun(@plus,position(j,1:2),bsxfun(@times,ranges,[cos(angles(i)),sin(angles(i))]));
        xy = coord2index(xy(:,1),xy(:,2),map_struct.scale,map_struct.min(1),map_struct.min(2));
        xy(:,1) = min(size(robo_map, 1)*ones(size(xy(:,1))), max(ones(size(xy(:,1))), xy(:,1)));
        xy(:,2) = min(size(robo_map, 2)*ones(size(xy(:,2))), max(ones(size(xy(:,2))), xy(:,2)));
        ind = unique(sub2ind(size(robo_map),xy(:,1),xy(:,2)));
        robo_map(ind) = robo_map(ind) + grid_params.log_odds_empty_sensor;
        if collision(j,i)>0
            ind = unique(sub2ind(size(robo_map),x(j,i),y(j,i)));
            robo_map(ind) = robo_map(ind) + grid_params.log_odds_occ_sensor;        
        end        
    end
end