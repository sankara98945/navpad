function [ wavefront_params ] = get_frontier_params(mode)
wavefront_params = struct();
wavefront_params.min_obstacle_distance=9;
wavefront_params.max_obstacle_distance=20;
wavefront_params.mode = mode;

end

