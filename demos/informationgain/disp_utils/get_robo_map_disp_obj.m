function display_obj = get_robo_map_disp_obj(robo_map, display_obj,first)
%GET_INFORMATION_DISP_OBJ Summary of this function goes here
%   Detailed explanation goes here
if (nargin<3)
    % Figure 1: Robo display
    error('get_robo_map_disp_obj needs a display obj');
elseif(first>0)
    display_obj.handle_set.fig_robo_view = figure;
    [~, p_] = logodds2prob(robo_map.data);
    display_obj.handle_set.occ_map = imshow(p_');
    axis equal;
else
    [~, p_] = logodds2prob(robo_map.data);
    set(display_obj.handle_set.occ_map,'CData', p_');
    axis equal
end


end
