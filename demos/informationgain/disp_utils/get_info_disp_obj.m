function [ display_obj ] = get_info_disp_obj( world_map, robo_map, nodes, state )
%GET_INFO_DISP_OBJ Summary of this function goes here
%   Detailed explanation goes here

display_obj = get_map_disp_obj(world_map);
display_obj = get_robo_map_disp_obj(robo_map,display_obj,1);
display_obj = get_nodes_disp_obj(nodes.pos, nodes.value, world_map,display_obj,1);

display_obj.handle_set.robo_state=[];
display_obj.handle_set.world_robo_state=[];
display_obj = get_robo_disp_obj(state,world_map, display_obj);

set(0,'CurrentFigure',display_obj.handle_set.fig_robo_view);
hold on
display_obj.handle_set.robo_view_chosen = plot_path( state);
display_obj.handle_set.robo_view_history = plot_history( state);

set(0,'CurrentFigure',display_obj.handle_set.fig_world);
hold on
display_obj.handle_set.world_chosen = plot_path( state);
display_obj.handle_set.world_history = plot_history( state);

display_obj.handle_set.robo_view_target_node = [];
display_obj.handle_set.world_view_target_node = [];
end
