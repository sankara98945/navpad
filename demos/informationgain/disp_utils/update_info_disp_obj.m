function [ display_obj ] = update_info_disp_obj(world_map, robo_map, nodes, state, history, chosen_path, selected_point, display_obj)
%UPDATE_INFO_DISP_OBJ Summary of this function goes here
%   Detailed explanation goes here

selected_point = coord2indexNotRound(selected_point(1),selected_point(2),...
                world_map.scale,world_map.min(1),world_map.min(2));

display_obj = get_map_disp_obj(world_map,display_obj);
display_obj = get_robo_map_disp_obj(robo_map,display_obj,0);
display_obj = get_nodes_disp_obj(nodes.pos, nodes.value, world_map,display_obj,0);
display_obj = get_robo_disp_obj(state,world_map, display_obj);

set(0,'CurrentFigure',display_obj.handle_set.fig_robo_view);
hold on
delete(display_obj.handle_set.robo_view_chosen);
delete(display_obj.handle_set.robo_view_history);
display_obj.handle_set.robo_view_chosen = plot_path( chosen_path);
display_obj.handle_set.robo_view_history = plot_history( history);
if(~isempty(display_obj.handle_set.robo_view_target_node))
    delete(display_obj.handle_set.robo_view_target_node)
end
display_obj.handle_set.robo_view_target_node = ...
plot(selected_point(1),selected_point(2),'o','MarkerSize',20);

set(0,'CurrentFigure',display_obj.handle_set.fig_world);
hold on
delete(display_obj.handle_set.world_chosen);
delete(display_obj.handle_set.world_history);
display_obj.handle_set.world_chosen = plot_path( chosen_path);
display_obj.handle_set.world_history = plot_history( history);
display_obj.handle_set.world_view_target_node = [];

end
